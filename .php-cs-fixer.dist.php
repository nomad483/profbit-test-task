<?php

declare(strict_types=1);

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var');

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'strict_param' => true,
        'array_syntax' => ['syntax' => 'short'],
        'blank_line_before_statement' => true,
        'concat_space' => ['spacing' => 'one'],
        'method_argument_space' => true,
        'single_trait_insert_per_statement' => true,
        'not_operator_with_successor_space' => false,
        'not_operator_with_space' => false,
        'phpdoc_single_line_var_spacing' => true,
        'phpdoc_align' => ['align' => 'left'],
        'declare_strict_types' => true,
    ])
    ->setFinder($finder);
