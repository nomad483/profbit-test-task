make: container.start

test:
	./bin/phpunit

container.start:
	docker-compose up -d

container.start.build:
	&& docker-compose up --build -d

container.restart:
	docker-compose down && docker-compose up -d

container.restart.build:
	docker-compose down && docker-compose up --build -d

start:
	symfony server:start

stop:
	symfony server:stop

csfixer.fix:
	vendor/bin/php-cs-fixer fix src

phpstan.analyse:
	 vendor/bin/phpstan analyse src tests

phpstan.fix:
	 vendor/bin/phpstan fix src tests

migrate:
	./bin/console --no-interaction doctrine:migration:migrate

migrate.test:
	./bin/console --no-interaction doctrine:migration:migrate --env=test

diff:
	./bin/console --no-interaction doctrine:migration:diff

load:
	./bin/console --no-interaction doctrine:fixtures:load

migrate.update: diff migrate

migrate.update.dev: diff migrate migrate.test

database.drop:
	./bin/console doctrine:database:drop --force

database.create:
	./bin/console doctrine:database:create --if-not-exists

database.drop.test:
	./bin/console doctrine:database:drop --force --env=test

database.create.test:
	./bin/console doctrine:database:create --env=test --if-not-exists

database.init: database.create migrate

database.init.dev: database.create database.create.test migrate migrate.test load

database.reinit.diff.dev: database.drop database.drop.test database.create database.create.test diff migrate migrate.test load
database.reinit.load: database.drop database.create migrate load

app.secret.generate:
	php bin/console regenerate-app-secret .env
