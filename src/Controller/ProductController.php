<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ProductController extends AbstractController
{
    public function __construct(private readonly ProductService $productService)
    {
    }

    #[Route('/products', name: 'product_index')]
    public function index(Request $request): Response
    {
        $pagination = $this->productService->getAllWithPagination(
            $request->query->get('sort', 'id'),
            $request->query->get('order', 'DESC'),
            (int) $request->query->get('page', 1),
            (int) $request->query->get('limit', 20)
        );

        return $this->render('product/index.html.twig', ['pagination' => $pagination]);
    }
}
