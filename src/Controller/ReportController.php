<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ReportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ReportController extends AbstractController
{
    public function __construct(private readonly ReportService $reportService)
    {
    }

    #[Route('/report', name: 'report_index')]
    public function index(): Response
    {
        $report = $this->reportService->getReport();

        return $this->render('report/index.html.twig', ['report' => $report]);
    }
}
