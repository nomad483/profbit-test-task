<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends BaseFixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 100; ++$i) {
            $product = (new Product())
                ->setCode(mt_rand(1, 10))
                ->setName($this->faker->word())
                ->setType($this->faker->randomElement(['type-1', 'type-2', 'type-3']))
                ->setPrice((string) mt_rand(100, 1000));

            $manager->persist($product);
        }

        $manager->flush();
    }
}
