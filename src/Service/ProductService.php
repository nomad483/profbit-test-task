<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

readonly class ProductService
{
    public function __construct(private ProductRepository $productRepository, private PaginatorInterface $paginator)
    {
    }

    /**
     * @return PaginationInterface<int, Product>
     */
    public function getAllWithPagination(string $field, string $order, int $page, int $limit): PaginationInterface
    {
        $queryBuilder = $this->productRepository->createQueryBuilder('p')->orderBy("p.$field", $order);

        return $this->paginator->paginate($queryBuilder, $page, $limit);
    }
}
