<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ProductRepository;

readonly class ReportService
{
    public function __construct(private ProductRepository $productRepository)
    {
    }

    public function getReport(): array
    {
        $products = $this->productRepository->findAll();

        $report = [];
        foreach ($products as $product) {
            $id = $product->getId();
            $code = $product->getCode();
            $type = $product->getType();
            $price = $product->getPrice();

            if (!isset($report[$code])) {
                $report[$code] = [
                    'total_count' => 0,
                    'total_price' => 0,
                    'types' => [
                        'type-1' => ['count' => 0, 'price' => 0],
                        'type-2' => ['count' => 0, 'price' => 0],
                        'type-3' => ['count' => 0, 'price' => 0],
                    ],
                ];
            }

            ++$report[$code]['total_count'];
            $report[$code]['total_price'] += $price;
            ++$report[$code]['types'][$type]['count'];
            $report[$code]['types'][$type]['price'] += $price;
        }

        uasort($report, fn ($a, $b) => $b['total_count'] - $a['total_count']);

        return $report;
    }
}
